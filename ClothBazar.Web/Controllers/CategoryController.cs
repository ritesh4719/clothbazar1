﻿using ClothBazar.Entities;
using ClothBazar.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ClothBazar.Web.Controllers
{
    public class CategoryController : Controller
    {
        CategoryServices categoryServices = new CategoryServices();
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Category category)
        {
            categoryServices.SaveCategory(category);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var category = categoryServices.GetCategory(id);
            return View(category);
        }
        [HttpPost]
        public ActionResult Edit(Category category)
        {
            categoryServices.UpdateCategory(category);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Index(Category category)
        {
            var categories = categoryServices.GetCategories();
            return View(categories);
        }
        [HttpGet]
        public ActionResult Delete(int id)
        {
            var category = categoryServices.GetCategory(id);
            return View(category);
        }
        [HttpPost]
        public ActionResult Delete(Category category)
        {
            categoryServices.DeleteCategory(category.ID);
            return RedirectToAction("Index");
        }
    }
}